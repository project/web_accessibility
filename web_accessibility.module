<?php

/**
 * @file
 * Contains web_accessibility.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_form_BASE_FORM_ID_alter() for \Drupal\node\NodeForm.
 */
function web_accessibility_form_node_form_alter(&$form, FormStateInterface $form_state) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $form_state->getFormObject()->getEntity();
  if (!$node->isNew()) {
    // Show web accessibility block only on published nodes.
    $form['web_accessibility_settings'] = [
      '#type' => 'details',
      '#title' => t('Web Accessibility Services'),
      '#open' => FALSE,
      '#group' => 'advanced',
      '#access' => TRUE,
      '#attributes' => ['class' => ['web-accessibility-form']],
      '#weight' => 30,
    ];

    $form['web_accessibility_settings']['content'] = [
      '#markup' => '<label>' . t('Check with') . '</label>',
    ];

    $nodeUrl = $node->toUrl('canonical', ['absolute' => TRUE])->toString();

    $services = \Drupal::service('web_accessibility.service_manager')
      ->findAll();
    if (!empty($services)) {
      foreach ($services as $service) {
        $serviceUrl = str_replace(\Drupal\web_accessibility\WebServiceInterface::URL_TOKEN, $nodeUrl, $service->url);
        $form['web_accessibility_settings']['services_table_' . $service->id] = [
          '#prefix' => '<div class="web_accessibility_link form-item">',
          '#type' => 'link',
          '#url' => Url::fromUri($serviceUrl, [
            'absolute' => TRUE,
            'attributes' => ['target' => '_blank']
          ]),
          '#title' => $service->name,
          '#suffix' => '</div>',
          '#attributes' => ['class' => ['button']],
        ];
      }
    }
  }
}

/**
 * Implements hook_help().
 *
 * @inheritdoc
 */
function web_accessibility_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.web_accessibility':
      $text = file_get_contents(__DIR__ . "/README.md");
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}
